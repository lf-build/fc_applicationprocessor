FROM registry.lendfoundry.com/base:beta8

ADD ./src/CapitalAlliance.GeoProfile.Abstractions /app/CapitalAlliance.GeoProfile.Abstractions
WORKDIR /app/CapitalAlliance.GeoProfile.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/CapitalAlliance.GeoProfile.Persistence /app/CapitalAlliance.GeoProfile.Persistence
WORKDIR /app/CapitalAlliance.GeoProfile.Persistence
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/CapitalAlliance.GeoProfile /app/CapitalAlliance.GeoProfile
WORKDIR /app/CapitalAlliance.GeoProfile
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build
ADD ./src/CapitalAlliance.GeoProfile.CsvDataImport.App /app/CapitalAlliance.GeoProfile.CsvDataImport.App
WORKDIR /app/CapitalAlliance.GeoProfile.CsvDataImport.App
VOLUME $(pwd)/csv:/var/www/csv
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build
ENTRYPOINT dnx run