﻿using System;
using System.Threading.Tasks;
using LendFoundry.Business.Application;
using LendFoundry.Foundation.Services;

namespace CapitalAlliance.ApplicationProcessor.Client
{
    public class ApplicationProcessorClientService : IApplicationProcessorClientService
    {
        public ApplicationProcessorClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        Task<IApplicationResponse> IApplicationProcessorClientService.SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            throw new NotImplementedException();
        }
    }
}
