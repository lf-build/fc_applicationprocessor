﻿using LendFoundry.Security.Tokens;

namespace CapitalAlliance.ApplicationProcessor.Client
{
    public interface IApplicationProcessorClientServiceFactory
    {
        IApplicationProcessorClientService Create(ITokenReader reader);
    }
}
