﻿using CreditExchange.StatusManagement.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using CreditExchange.Datamerch.Client;
//using CreditExchange.Whitepages.Client;
//using CreditExchange.Yelp.Client;
using CreditExchange.BBBSearch.Client;

namespace CapitalAlliance.ApplicationProcessor.Api
{
    public class Startup
    {

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddConfigurationService<ApplicationProcessorConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);

            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<ApplicationProcessorConfiguration>>().Get());

            services.AddLookupService(Settings.LookUpService.Host, Settings.LookUpService.Port);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddApplicationService(Settings.BusinessApplication.Host, Settings.BusinessApplication.Port);

            services.AddDatamerchService(Settings.DataMerchSyndication.Host, Settings.DataMerchSyndication.Port);
            //services.AddWhitepagesService(Settings.WhitePagesSyndication.Host, Settings.WhitePagesSyndication.Port);
            //services.AddYelpService(Settings.WhitePagesSyndication.Host, Settings.WhitePagesSyndication.Port);
            services.AddBBBSearchService(Settings.BBBSearchSyndication.Host, Settings.BBBSearchSyndication.Port);

            services.AddTransient<IApplicationProcessorClientService, ApplicationService>();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseEventHub();
            //app.UseScoreCardListener();
            app.UseMvc();
        }
    }
}
