﻿using LendFoundry.Business.Applicant;
using System;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface ISubmitBusinessApplicationRequest
    {
        string Email { get; set; }
        double RequestedAmount { get; set; }
        string PurposeOfLoan { get; set; }
        DateTimeOffset DateNeeded { get; set; }
        string LegalBusinessName { get; set; }
        string AddressLine1 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PostalCode { get; set; }
        int AddressType { get; set; }
        string BusinessTaxID { get; set; }
        string SICCode { get; set; }
        DateTimeOffset BusinessStartDate { get; set; }
        BusinessType BusinessType { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string SSN { get; set; }
        string DateOfBirth { get; set; }        
        string Ownership { get; set; }
        string HomeAddressLine1 { get; set; }
        string HomeCity { get; set; }
        string HomeState { get; set; }
        string HomePostalCode { get; set; }
        string Mobile { get; set; }
        double AnnualRevenue { get; set; }
        double AverageBankBalances { get; set; }
        bool HaveExistingLoan { get; set; } 
    }
}
